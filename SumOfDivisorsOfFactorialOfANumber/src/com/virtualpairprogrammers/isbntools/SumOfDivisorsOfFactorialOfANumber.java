package com.virtualpairprogrammers.isbntools;

//import jdk.swing.interop.SwingInterOpUtils;

public class SumOfDivisorsOfFactorialOfANumber {

    private static read readnumber = new read();
    private static int n;

    SumOfDivisorsOfFactorialOfANumber() throws Exception {

    }

    public void setreadobject(read file) {
        this.readnumber = file;
    }

    public static int sumOfDivisorsOfAFactorialOfANumber() throws Exception {
        n = readnumber.reader("test.txt");
        System.out.println("Value from the input file =" + n);

        int fact = 1;
        for (int i = 1; i <= n; i++) {
            fact = fact * i;
        }

        int ans = 0;
        for (int i = 1; i <= fact; i++)
            if (fact % i == 0)
                ans += i;
       // System.out.println("The sum of divisors of factorial of " + n + " is " + ans);
    return ans;
    }

}
